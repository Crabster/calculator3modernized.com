import java.util.Scanner;

public class Calculator3modernized {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String arr[] = new String[5];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Введите тип чисел для последующих расчетов: 1-целые числа, 2-дробные числа, 3-операторы сравнения, 4-завершить работу");
            var type = scan.nextInt();
            switch (type) {
                case 1:             //блок целых чисел
                    System.out.println("Введите первое число");
                    var a = scan.nextInt();
                    System.out.println("Введите второе число");
                    var b = scan.nextInt();
                    System.out.println("Введите действие: 0-сумма, 1-разность, 2-произведение, 3-деление, 4-сдвиг влево, 5-сдвиг вправо, 6-сдвиг вправо с заполнением нулями, 7-& побитовое 'И', 8-| побитовое 'ИЛИ'");
                    var operation1 = scan.nextInt();
                    if (b == 0 && operation1 == 3) {
                        do {
                            System.out.println("Введите любое значение кроме 0");
                            b = scan.nextInt();
                        } while (b == 0);
                    }
                    int acc = 0;
                    switch (operation1) {
                        case 0:
                            acc = a + b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 1:
                            acc = a - b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 2:
                            acc = a * b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 3:
                            acc = a / b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 4:
                            acc = a << b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 5:
                            acc = a >> b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 6:
                            acc = a >>> b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 7:
                            acc = a & b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        case 8:
                            acc = a | b;
                            System.out.println("Результат: " + acc);
                            arr[i] = "Результат равен: " + acc;
                            break;
                        default:
                            System.out.println("Вычисление невозможно");
                            arr[i] = "Вычисление невозможно";
                            break;
                    }
                    break;
                case 2:                      //блок дробных чисел
                    System.out.println("Введите первое число");
                    float k = scan.nextFloat();
                    System.out.println("Введите второе число");
                    float j = scan.nextFloat();
                    System.out.println("Введите действие: 0-сумма, 1-разность, 2-произведение, 3-деление");
                    var operation2 = scan.nextInt();
                    if (j == 0 && operation2 == 3) {
                        do {
                            System.out.println("Введите любое значение кроме 0");
                            j = scan.nextInt();
                        } while (j == 0);
                    }
                    float acc1 = 0;
                    switch (operation2) {
                        case 0:
                            acc1 = k + j;
                            System.out.println("Результат: " + acc1);
                            arr[i] = "Результат равен: " + acc1;
                            break;
                        case 1:
                            acc1 = k - j;
                            System.out.println("Результат: " + acc1);
                            arr[i] = "Результат равен: " + acc1;
                            break;
                        case 2:
                            acc1 = k * j;
                            System.out.println("Результат: " + acc1);
                            arr[i] = "Результат равен: " + acc1;
                            break;
                        case 3:
                            acc1 = k / j;
                            System.out.println("Результат: " + acc1);
                            arr[i] = "Результат равен: " + acc1;
                            break;
                        default:
                            System.out.println("Вычисление невозможно");
                            arr[i] = "Вычисление невозможно";
                            break;
                    }
                    break;
                case 3:                     //блок логических операторов
                    System.out.println("Введите первый операнд в формате true или false");
                    boolean m = scan.nextBoolean();
                    System.out.println("Введите второй операнд в формате true или false");
                    boolean p = scan.nextBoolean();
                    System.out.println("Введите действие сравнения: 0-&& логическое 'И', 1-|| логическое 'ИЛИ', 2-^ логическое исключающее 'ИЛИ'");
                    var operation3 = scan.nextInt();
                    boolean acc2 = true;
                    switch (operation3) {
                        case 0:
                            acc2 = m && p;
                            System.out.println("Результат: " + acc2);
                            arr[i] = "Результат равен: " + acc2;
                            break;
                        case 1:
                            acc2 = m || p;
                            System.out.println("Результат: " + acc2);
                            arr[i] = "Результат равен: " + acc2;
                            break;
                        case 2:
                            acc2 = m ^ p;
                            System.out.println("Результат: " + acc2);
                            arr[i] = "Результат равен: " + acc2;
                            break;
                        default:
                            System.out.println("Вычисление невозможно");
                            arr[i] = "Вычисление невозможно";
                            break;
                    }
            }
            if (type < 1 | type > 4) {
                System.out.println("Вычисление невозможно");
                arr[i] = "Вычисление невозможно";
            }
            if (type == 4) {
                System.out.println("Вычисление прервано по требованию");
                arr[i] = "Вычисление прервано по требованию";
                break;
            }
            if (i == 4) {
                System.out.println("Совершено 5 последовательных операций. Устройство перегрелось");
            }
        }
        while (true) {                //блок работы с архивом
            System.out.println("Если хотите воспользоваться архивом с результатами, выберите номер операции от 1 до 5. Если же не хотите - нажмите 0");
            int archive = scan.nextInt();
            if (archive < 0 || archive > 5) {
                do {
                    System.out.println("Вы ввели некорректный номер операции! Снова выберите номер операции от 1 до 5. Для выхода нажмите 0");
                    archive = scan.nextInt();
                } while (archive < 0 || archive > 5);
            } else {

                if (arr[archive - 1] != null) {
                    switch (archive) {
                        case 1:
                            System.out.println(arr[archive - 1]);
                            break;
                        case 2:
                            System.out.println(arr[archive - 1]);
                            break;
                        case 3:
                            System.out.println(arr[archive - 1]);
                            break;
                        case 4:
                            System.out.println(arr[archive - 1]);
                            break;
                        case 5:
                            System.out.println(arr[archive - 1]);
                            break;
                    }
                } else {
                    System.out.println("Операция не производилась, архивные данные отсутствуют. Работа калькулятора была досрочно завершена");
                }
            }
            if (archive == 0) {
                System.out.println("Работа с архивом прервана по требованию");
                break;
            }
        }
    }
}








